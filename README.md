﻿## Identidad Institucional e Imagen de Marca



# Licencias y atribuciones

- Identidad Institucional

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">identidad institucional</span> por <a xmlns:cc="http://creativecommons.org/ns#" href="www.rootdevel.co" property="cc:attributionName" rel="cc:attributionURL">Oscar Reyes, Fabian Salamanca, Leonardo Alvarado</a> se distribuye bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional</a>. 

- Imagen de Marca

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/StillImage" property="dct:title" rel="dct:type">Logo Rootdevel Hackepsace</span> por <a xmlns:cc="http://creativecommons.org/ns#" href="www.rootdevel.co" property="cc:attributionName" rel="cc:attributionURL">Edgard Joya Cepeda</a> se distribuye bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional</a>.
