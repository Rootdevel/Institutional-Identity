﻿## Logo Rootdevel Hackerspace

 Esta obra está licenciada bajo la Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional. Para ver una copia de esta licencia, visita http://creativecommons.org/licenses/by-nc-nd/4.0/.
 
# Attributions
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Licencia Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/StillImage" property="dct:title" rel="dct:type">Logo Rootdevel Hackerspace</span> por <a xmlns:cc="http://creativecommons.org/ns#" href="www.rootdevel.co" property="cc:attributionName" rel="cc:attributionURL">Edgard Joya Cepeda</a> se distribuye bajo una <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Licencia Creative Commons Atribución-NoComercial-SinDerivar 4.0 Internacional</a>.

